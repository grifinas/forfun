# parses inputed function and calculates the answer
# formula.py (4+7) * 8 + 2 ^ 4
# 104

import sys

def isAction(string):
    return string == '+' or string == '-' or string == '/' or string == '*' or string == '^'

def actionSort(actions):
    powers, multi, add = [], [], []
    for key, item in actions.items():
        if item == '^':
            powers.append((key, item))
        elif item == '*' or item == '/':
            multi.append((key, item))
        else:
            add.append((key, item))
    return powers + multi + add


def getNum(nst, pos):
    while(nst[pos] == '_'):
        pos -= 1
    return nst[pos], pos

def compile(nst, ast):
    actions = actionSort(ast)
    for spot, action in actions:
        x, x_pos = getNum(nst, spot)
        y, y_pos = getNum(nst, spot+1)
        if action == '^':
            result = x**y
        elif action == '*':
            result = x*y
        elif action == '/':
            result = float(x)/y
        elif action == '+':
            result = x+y
        elif action == '-':
            result = x-y
        else:
            Exception("Unknown opperator mixed in into compile time")
        print x, action, y, '=', result
        nst[y_pos] = '_'
        nst[x_pos] = result
    return nst[0]


def parse(fn):
    number_stack = []
    action_stack = {}
    i = 0
    num = 0
    writing_num = False
    while(len(fn) > i):
        letter = fn[i]
        i +=1
        if letter.isdigit():
            writing_num = True
            num = num*10+int(letter)
        elif writing_num:
            number_stack.append(num)
            writing_num = False
            num = 0

        if writing_num == False:
            if isAction(letter):
                spot = len(number_stack)-1
                if spot <= 0:
                    Exception("Operator " + letter + " before any numbers")
                action_stack[spot] = letter
            elif letter == '(':
                num_i = parse(fn[i:])
                i += num_i[1]
                number_stack.append(num_i[0])
            elif letter == ')':
                break
            else:
                print "Urecognised symbol:", letter

    if writing_num:
        number_stack.append(num)
        writing_num = False
        num = 0

    return compile(number_stack, action_stack), i


def calculate(fn):
    result = parse(fn)[0]
    if abs(result - int(result)) <= sys.float_info.epsilon:
        result = int(result)
    return result

if (len(sys.argv) == 2):
    fn = sys.argv[1]
else:
    print "Please input function"
    fn = raw_input()
res = calculate(fn)
print "Answer:", res